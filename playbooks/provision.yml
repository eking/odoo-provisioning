---
- name: provision
  hosts: odoo_servers
  become: yes
  vars:
    - nginx_extra_params:
      - client_max_body_size 200M
  roles:
    - role: inventory_version_check

    - role: network_config

    - role: geerlingguy.security

    - role: geerlingguy.postgresql
      vars:
        postgresql_python_library: python3-psycopg2

    - role: postgresql-odoo
      vars:
        postgres_odoo_dbs: "{{  odoo_role_odoo_dbs  or [ odoo_role_odoo_db_name ] }}"

    - role: coopdevs.certbot_nginx
      vars:
        domain_name: "{{ domains | default([inventory_hostname]) | join(',') }}"
        letsencrypt_email: "{{ certificate_authority_email }}"
        certbot_nginx_cert_name: "{{ certbot_cert_name | default(inventory_hostname) }}"
        # Force creating a new certificate when we have multiple domains like in multi-db
        certbot_force_update: "{{ domains | count > 1 }}"
      when: not development_environment
      tags: nginx

    - role: basic_auth
      when:
        - not development_environment
        - basic_auth_users is defined
      tags: nginx

    - role: jdauphant.nginx
      nginx_http_params: "{{ nginx_http_default_params + nginx_extra_params }}"
      when: not development_environment
      tags: nginx

    - role: coopdevs.odoo-role
      # Set these vars automatically ignoring values set in inventory.
      # Decide using odoo-provisioning variable development_environment
      vars:
        odoo_role_odoo_http_interface: "{{ '0.0.0.0' if development_environment else '127.0.0.1' }}"
        odoo_role_odoo_proxy_mode: "{{ 'false' if development_environment else 'true' }}"
        odoo_role_db_port: "{{ expose_postgresql_port | default(false) }}"
      tags: odoo-role

    - role: expose-postgresql
      become_user: postgres
      tags: exposedb

    - role: backups
      when: backups_role_enabled

    - role: coopdevs.monitoring_role
      when:
        - monitoring_enabled | default(True)
        - not development_environment
      tags: monitoring

    - role: geerlingguy.firewall
      vars:
        firewall_allowed_tcp_ports:
          - "22"  # for SSH
          - "80"  # for HTTP. Nginx redirects it to 443
          - "443"  # for HTTPS.
      tags: firewall
      when: not development_environment
