# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [v0.6.2] - 2020-02-26
- Disable firewall in development environment
  See [!139](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/139)

## [v0.6.1] - 2020-02-25
- Upgrade odoo-role to v0.2.18 (Set ribbon name param for test environment and fix default db name to `odoo`)
  See [!136](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/136)

## [v0.6.0] - 2020-02-24
- Install and configure a firewall
  See [!134](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/134)

## [v0.5.17] - 2020-02-10
- Upgrade odoo-role to v0.2.16 (Add libjpeg-dev and python-stdnum==1.9 as new deps for v11)
  See [!130](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/130)

## [v0.5.16] - 2020-02-10
- Upgrade odoo-role to v0.2.15 (Don't start HTTP server in Odoo modules init task)
  See [!128](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/128)
- Add no_log to the DB user create task in expose-postgresql role
  See [!127](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/127)

## [v0.5.15] - 2020-01-28
- Upgrade odoo-role to v0.2.14 (Don't start HTTP server in Odoo modules update task and fix odoo\_role\_workers not defined error)
  See [!125](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/125)

## [v0.5.14] - 2020-01-27
- Add tag to exposedb role
  See [!122](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/122)
- Upgrade odoo-role to v0.2.13 (Customize the Odoo timeouts)
  See [!123](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/123)

## [v0.5.13] - 2020-01-14
- Added feature for copying Odoo Backup to SFTP server
  See [!119](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/119)

## [v0.5.12] - 2020-01-10
- Upgrade odoo-role to v0.2.12 (Add workers and channels support)
  See [!117](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/117)

## [v0.5.11] - 2021-01-08
- Allow creation of multiple basic authentication users
  See [!115](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/115)

## [v0.5.10] - 2021-01-05
- Upgrade monitoring-role to v0.3.1 (Fix Promptail configurations).
  See [!113](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/113)
- Restart PostgreSQL after change the pg_hba entries.
  See [!112](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/112)

## [v0.5.9] - 2020-12-17
- Upgrade odoo-role to v0.2.11 (Manage the DB port in Odoo config) and add a role to expose PostgreSQL to TCP/IP connections.
  See [!111](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/111)
- Upgrade odoo-role to v0.2.10 (Flag to enable dbfilter\_from\_header module)
  See [!109](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/109)
- Upgrade Certbot Nginx Role to v0.2.1 (Updated cerbot package in Ubuntu)
  See [!108](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/108)

## [v0.5.8] - 2020-10-15
- Upgrade odoo-role to v0.2.9 (Add python-magic apt requirement, fix the linter pipeline)
  See [!106](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/106)

## [v0.5.7] - 2020-10-08
- Iterate over domains adding them to /etc/hosts
  See [!104](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/104)
- Upgrade odoo-role to v0.2.8 (FIX: environment\_variable condition in the service template)
  See [!105](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/105)

## [v0.5.6] - 2020-09-23
- Upgrade odoo-role to v0.2.7 (Add envvars management)
  See [!103](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/103)

## [v0.5.5] - 2020-09-07
- Upgrade odoo-role to v0.2.6 (FIX: list\_db must be True if queue\_job enabled)
  See [!101](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/101)

## [v0.5.4] - 2020-09-03
- Upgrade odoo-role to v0.2.5 (Add Rest Framework support dev mode, add option to enable queue\_job module)
  See [!100](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/100)

## [v0.5.3] - 2020-09-01
- Add Basic Auth
  See [!99](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/99)

## [v0.5.2] - 2020-08-27
- Removed Postgresql password
  See [!97](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/97)
- Install unaccent extension in Postgresql and upgraded Odoo Role to v0.2.4 (unaccent in Odoo conf, avoid LC\_COLLATE="C")
  See [!98](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/98)

## [v0.5.1] - 2020-06-10
### Added
- Inc post body size
  See [!94](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/94)
- Upgrade odoo-role to v0.2.2
  See [!95](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/95)

## [v0.5.0] - 2020-06-08
### Added
- Support for Ubuntu 20.04
  See [!92](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/92)

## [v0.4.1] - 2020-04-24
### Added
- New role to add network settings.
  See [!88](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/88)

### Changed
- Add bash function that has been deleted from main script. Due to this backups were not working.
  See [!90](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/90)

## [v0.4.0] - 2020-01-28
### Added
- Create CHANGELOG.md with history from Gitlab releases.
  See [!85](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/85)
- Add [monitoring role](https://github.com/coopdevs/monitoring-role/tree/v0.3.0) for production environments. Defaults to yes.
  Please add the secrets described at [secrets-example.yml](https://github.com/coopdevs/monitoring-role/tree/add/promtail-odoo-template/defaults)
- Add pyenv config. If you already have your environment working, You  may want
  to set pyenv up to sync with upstream requirements and possible changes.
  See [!83](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/83)

### Changed
- Upgrade certbot_nginx to [v0.1.0](https://github.com/coopdevs/certbot_nginx/releases/tag/v0.1.0).
- Use certbot_nginx new flag to create new certs when in multidomain
- Upgrade odoo-role to [v0.1.8](https://github.com/coopdevs/odoo-role/releases/tag/v0.1.8).
  No more deprecation warnings in up-to-date inventories.
- Upgrade backups-role to [v1.2.6](https://github.com/coopdevs/backups_role/releases/tag/v1.2.6)
  Logging compatible with monitoring-role

### Removed
- Bind interface and proxy mode variables are now set automatically. Remove them
  from your inventory, please.
  See [!84](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/84)

## [v0.3.2] - 2019-09-16
### Changed
- [!82](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/82) Upgrade odoo role. Fixes upgrading comm. modules
- [!81](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/81) Fix typo in README.md

## [v0.3.1] - 2019-09-12
### Changed
- [!80](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/80) Enrich deprecation messages and add odoo_role_odoo_db_name
- [!79](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/79) Hotfix dbfilter: we roll back to db filtering only by subdomains %d, which has its limitations but works.
- [!78](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/78) Improve README

## [v0.3.0] - 2019-08-30
### Changed
- fea677cb Upgrade odoo-role to v0.1.5
- c4e104a6 and 1425c536 Update README.md

### Removed
- [!77](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/77)  Deprecate confusing vars

## [v0.2.3] - 2019-08-19
### Changed
- 4ca243c7 Hotfix for left-over deprecated var. Untested from 2c2d06a7.
- 2c2d06a7 Hotfix for some untested cases from [!76](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/76)
- [!76](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/76) Upgrade odoo-role from v0.1.2 to v0.1.3
- [!74](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/74) Import backups role instead of including. Otherwise --tags=install doesn't work
- [!69](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/69) Review odoo role and postgres vars (merged via cli)

## [v0.2.2] - 2019-07-16
### Changed
- [!73](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/73) Upgrade odoo-role version for multi-db full support
- [!72](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/72) Use domain_name and certbot_nginx_cert_name to generate multidomain certificate
- [!71](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/71) Upgrade odoo-role version. Multi DB related
- [!70](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/70) Use domains list instead a domain string in cerbot_nginx vars
- [!67](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/67) Adapt internal postgres role to multiple dbs

## [v0.2.1] - 2019-06-20
### Changed
- [!65](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/65)

## [v0.2.0] - 2019-05-14
### Changed
Among others, check that inventories are up-to-date with the playbooks version.

- [!59](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/59)
- [!61](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/61)
- [!62](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/62)
- [!63](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/63)
- [!64](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/64)

## [v0.1.0] - 2019-04-10
### Added
- First working version using backups_role from Ansible Galaxy

## [v0.0.2] - 2019-04-03
### Changed
- Fix critical requirements typo.

## [v0.0.1] - 2019-03-28
### Added
- First release! :rocket:

