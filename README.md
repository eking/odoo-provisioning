# Ansible scripts to provision and deploy Odoo

These are [Ansible](http://docs.ansible.com/ansible/) playbooks (scripts) for managing an [Odoo](https://github.com/odoo/odoo) server.

## Requirements

You will need Ansible on your machine to run the playbooks.
These playbooks will install the PostgreSQL database, NodeJS and Python virtualenv to manage python packages.

It has currently been tested on hosts running

* Ubuntu 18.04 **Bionic** (amd64)
* Ubuntu 16.04 **Xenial** (amd64)

### Environment requirements

In order to assure that the playbooks contained here are run always with the same ansible version and linted the same way, we are using [`pyenv`](https://github.com/pyenv/pyenv).

Follow [Installing and using pyenv](https://github.com/coopdevs/handbook/wiki/Installing-and-using-pyenv), or, in short:

- [ ] Install pyenv (the script-based way is simple) `curl https://pyenv.run | bash`
- [ ] Install a local python version. Choose **`3.7`** `pyenv install 3.7`
- [ ] Create a virtual env with pyenv for this project `pyenv virtualenv 3.7 odoo-provisioning`
- [x] Activate the venv (this is automatic in pyenv when you cd into the dir with .python-version)
- [ ] Install required tools with local pip `(odoo-provisioning) $ pip install -r requirements.txt`

### Role requirements

Install dependencies running:
```
ansible-galaxy install -r requirements.yml
```

## Inventory

You need to define an inventory folder with the `hosts` file and the `group_vars` and `host_vars` file. See the [Ansible doc about inventories](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html).
For more dettails on how to create a compatible inventory, see the [wiki](https://gitlab.com/coopdevs/odoo-provisioning/wikis/How%20to%20create%20a%20new%20Odoo%20instance).

## Playbooks

### sys_admins.yml

This playbook uses the community role [sys-admins-role](https://github.com/coopdevs/sys-admins-role).

This playbook will prepare the host to allow access to all the system administrators.

```yaml
# playbooks/my_playbook.yml
- name: Create all the users for system administration
  roles:
    - role: coopdevs.sys-admins-role
      vars:
        sys_admin_group: sysadmin
        sys_admins: "{{ system_administrators }}"
```

In each environment (`dev`, `staging`, `production`) we can find the list of users that will be created as system administrators.

We use `host_vars` to declare per environment variables:
```yaml
# <YOUR_INVENTORY_FOLDER>/inventory/host_vars/<YOUR_HOST>/config.yml

system_administrators:
  - name: pepe
    ssh_key: "../pub_keys/pepe.pub"
    state: present
    - name: paco
    ssh_key: "../pub_keys/paco.pub"
    state: present
```

Before execute the playbook, be sure that the needed keys are present in `pub_keys` folder.
The first time you run it against a brand new host you need to run it as `root` user.
You'll also need passwordless SSH access to the `root` user.
```
ansible-playbook playbooks/sys_admins.yml --limit=<environment_name> -u root -i <YOUR_INVENTORY_FILE>
```

For the following executions, the script will assume that your user is included in the system administrators list for the given host.

For example in the case of `development` environment the script will assume that the user that is running it is included in the system administrators [list](https://github.com/coopdevs/timeoverflow-provisioning/blob/master/inventory/host_vars/local.timeoverflow.org/config.yml#L5) for that environment.

To run the playbook as a system administrator just use the following command:
```
ansible-playbook playbooks/sys_admins.yml --limit=dev
```
Ansible will try to connect to the host using the system user. If your user as a system administrator is different than your local system user please run this playbook with the correct user using the `-u` flag.
```
ansible-playbook playbooks/sys_admins.yml --limit=dev -u <username>
```

### provision.yml

Installs and configures all required software on the server.

This is the main playbook. This playbook install all the Odoo dependencies and the Odoo version that you define in the inventory. Install and update also the modules defined in the inventory repository.

The playbook use different community roles to perform the complete Odoo installation. Also use our own roles to manage the user and database creation.

#### Basic Authentication

In some projects we expose a public endpoint and need to secure it with Basic Authentication. The reason for this could be because the tool that uses the endpoint only allows to use a Basic Auth (for example OTRS).

You can define the var `basic_auth_users` like the following example:
```yaml
basic_auth_users:
  - { 'path': '/etc/nginx/.htpasswd', 'name': 'username', 'password': 'passw0rd' }
  - { 'path': '/etc/nginx/.other.htpasswd', 'name': 'username2', 'password': 'passw0rd2' }
```
A good practice is to store the password in a separated variable in a Ansible vault.

Now you need to configure the basic auth in your NGINX servers configuration.

### restore.yml

Restores locally the latest snapshot of the intended target host.

Needs sudo to install restic, so execute it with `--ask-become-pass`.

## Community roles:
* Security - [geerlingguy.security](https://github.com/geerlingguy/ansible-role-security)

This role changes the minimum security configuration. More information in the role description.

* PostgreSQL - [geerlingguy.postgresql](https://github.com/geerlingguy/ansible-role-postgresql)

Role to install PostgreSQL in the different environments.

* Certbot NGINX - [coopdevs.certbot_nginx](https://github.com/coopdevs/certbot_nginx)

This role install and configure the NGINX plugin of Certbot to allow renew the certificate when NIGNX is running.
Alto create the certificate if it not exists
More info in the role description.

You need declare the next variables:
```yaml
certificate_authority_email:        # Email to associate the certificate
```
In case of using multiple DB, database names must match subdomains. Define:
```yaml
domains:
  - default.example.org
  - group1.example.org
  - group2.example.org

odoo_role_odoo_dbs:
  - default
  - group1
  - group2
```


* NGINX - [jdauphant.nginx](https://github.com/jdauphant/ansible-role-nginx)

Install NGINX package in different environments.

* Odoo - [coopdevs.odoo-role](https://github.com/coopdevs/odoo-role)

This is the role that install and configure the Odoo server.
You need to declare the next vars:
```yaml
# Secrets
odoo_role_odoo_db_admin_password:   # DB Master password.

# Config
odoo_role_odoo_core_modules:        # List of modules to install from Odoo core.
odoo_role_odoo_community_modules:   # List of modules to install from Odoo community.
odoo_role_odoo_dbs:                 # Array of database names. If more than one, names must match with
                                    #  lefmost part of the hostname (but ignoring www if present), from which each DB will be used.
odoo_role_download_strategy:        # Either "tar" or "git"
odoo_role_odoo_release:             # Only with tar. Name of the release to use in odoo_url and odoo_download_path.
odoo_role_odoo_url:                 # Only with tar. URL of the release file.
odoo_role_odoo_git_ref:             # Only with git. Branch, tag, commit or any other valid git reference to checkout.
odoo_role_odoo_git_url:             # Only with git. URL of the git repository to clone.
```

Regarding download strategies, we mantain a mirror of the [community version of Odoo](https://github.com/OCA/OCB), where [we publish releases](https://gitlab.com/coopdevs/OCB/-/releases) when needed. Keep in mind that OCB git repository changes its git history from time to time and also don't publish tags anymore, so it's not safe to rely on them for version freezing.

Thus, we recommend the following settings:

```yaml
odoo_role_download_strategy:  "tar"
odoo_role_odoo_release: "11.0_2019-05-05" # or newer. Find them at https://gitlab.com/coopdevs/OCB/-/releases
odoo_role_odoo_url: "https://gitlab.com/coopdevs/OCB/-/archive/{{ odoo_role_odoo_release }}/OCB-{{ odoo_role_odoo_release }}.tar.gz"
# odoo_role_odoo_git_url:
# odoo_role_odoo_git_ref:
```

> To install the modules, first you need to be sure that the modules are in the scope of Odoo. You can deploy the modules in the server with `pip`. To the community roles, you need define a `requirements.txt` inside the *inventory* folder.

* Monitoring - [coopdevs.monitoring-role](https://github.com/coopdevs/monitorin-role)

We want to have visibility of what happens at the instance that Odoo is running. To this end, we collect odoo logs, backups logs, and metrics from the system such as free disk space or RAM usage.

You need to declare the next vars:

```yaml
# Regular variables
monitoring_enabled: true
monitoring_nexporter_enabled: true
monitoring_promtail_enabled: true
monitoring_promtail_modules_enabled:
  - backups
  - odoo
```
And the following secrets, in case you enable promtail:
```yaml
# Secret variables
monitoring_loki_user: "1234"
# this value is the base64 encoding of '{"k":"3eefc66d8e48fa704fd0f0a4c3e51534cd7c4647f","n":"your grafana cloud key","id":123456}'
# but it's generated by grafana cloud. See https://github.com/coopdevs/handbook/wiki/Grafana-cloud-credentials
monitoring_loki_hostname: "logs-somewhere.grafana.net"
monitoring_loki_key: 'eyJrIjoiM2VlZmM2NmQ4ZTQ4ZmE3MDRmZDBmMGE0YzNlNTE1MzRjZDdjNDY0N2YiLCJuIjoieW91ciBncmFmYW5hIGNsb3VkIGtleSIsImlkIjoxMjM0NTZ9'
monitoring_loki_hostname: "logs-somewhere.grafana.net"
```

In the case of node exporter you also need to create a Basic Authentication user and a new `/location` in NGNIX in order to expose the node exporter endpoint.

## Roles
* PostgreSQL Odoo - `postgresql-odoo`

This role is a workaround to create the database and the user with the needed permissions.
You need define the next vars:
```yaml
odoo_role_odoo_dbs:                           # DB names to create. Always a list, even if only 1 DB is desired
odoo_role_odoo_db_user:                       # DB user
```

> To avoid install NGINX and the NGINX Certbot plugin, we can define a var as:
```yaml
development_environment: true
```

* Backups - `backups`

This role overwrites the strategy to obtain data to be backed up, so that we use Odoo's own way.
You need to define these variables, consider doing it in an Ansible vault. Example values.
```yaml
# Remote bucket URL in restic format
# Example for backblaze:  "b2:bucketname:path/to/repo"
# Example for local repo: "/var/backups/repo"
backups_role_restic_repo_url: "b2:unique-bucket-name:path/to/restic/repo/inside/the/bucket"
backups_role_restic_repo_password: "rwqlenwhat7a7rand0m7passw0rd7ss2"

# Backblaze "application" or bucket credentials
backups_role_b2_app_key_id: "00f00ba12bebo000000000023"
backups_role_b2_app_key: "uyoWyGV8ueNUW0ea98fkPAN1CxMKijK"
```

Backups are enabled by default, but can be disabled by setting `backups_role_enabled: false`. You may want to do it at your testing instance, or for dev environments. As an alternative to host vars files, you can also use the ansible command-line paramenter `--extra-vars`, or `-e` for short.

```sh
# Append this option to your ansible-playbook call
#+ in order to disable backups for this play.
-e '{backups_role_enabled: false}'
```

Note: there is a simpler syntax in the form `-e "key=value"`, but it only supports strings and the role condition expects a boolean. This means that it will not tell the difference between non-empty strings, e.g. `"true"` and `"false"` both evaluate to `true`. This is why we use the JSON syntax, that allows booleans. It's ok for this JSON parser to avoid quotes on the keys.

## Initial set-up

Check the wiki page [How to create a new Odoo instance](https://gitlab.com/coopdevs/odoo-provisioning/wikis/How%20to%20create%20a%20new%20Odoo%20instance)

## Installation instructions

### Step 1 - sys_admins

The **first time** to execute this playbook use the user `root`. You will need SSH access to the machine and python3 installed.

`ansible-playbook playbooks/sys_admins.yml --limit <environment_name> -u root`

All the next times use your personal system administrator user:

`ansible-playbook playbooks/sys_admins.yml --limit <environment_name> -u USER`

USER --> Your sysadmin user name.

### Step 2 - provision

`ansible-playbook playbooks/provision.yml -u USER`

USER --> Your sysadmin user name.

## Users Management

### Default User `odoo`

It is the owner of the `odoo` group and can execute the next commands without password in `sudo` mode:

```
sudo systemctl start odoo.service
sudo systemctl stop odoo.service
sudo systemctl status odoo.service
sudo systemctl restart odoo.service
```

**It can execute the Odoo service and restart if is needed. Is the user to manage the Odoo process and to install and update new modules.**

### System Administrators

The sysadmins are the superusers of the environment.
They have `sudo` access without password for all commands.

**They can execute `sys_admins.yml` and `provision.yml` playbooks.**

## DB Admin Password

Add password to the `super admin` that manage the dbs.
In `inventory/host_vars/host_name/secrets.yml` add the key `admin_passwd` to protect the creation and management of dbs.
`secrets.yml` is a encrypted vault. Run `ansible-vault edit secrets.yml` to change this password.

# Development

In the development environment (`odoo.local`) you must use the sysadmin user `odoo`.

`ssh odoo@local.odoo.net`

## Using LXC Containers

In order to create a development environment, we use the [devenv](https://github.com/coopdevs/devenv) tool. It uses [LXC](https://linuxcontainers.org/) to isolate the development environment.

This tool searches a configuration dotfile in the working directory. Please create one in your inventory if you want to take advantatge of this tool.

You can copy the simple [odoo-test .devenv file](https://gitlab.com/coopdevs/odoo-test-provisioning/blob/master/.devenv) or explore a bit more at the [devenv project site](https://github.com/coopdevs/devenv).
